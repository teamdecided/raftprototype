﻿namespace TeamDecided.RaftPrototype.Enums
{
    public enum EServerState
    {
        ACTIVE = 0,
        INACTIVE = 1,
        OFFLINE = 2
    }
}
