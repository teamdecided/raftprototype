﻿using System;
using System.Windows.Forms;
using TeamDecided.RaftPrototype.Enums;
using TeamDecided.RaftPrototype.Models;

namespace TeamDecided.RaftPrototype.Interfaces
{
    public interface IPresent
    {
        IPresent GetInstance();
        Form GetView();
        RaftConfigurationModel GetModel();
        void Show();
        void Close();

        event EventHandler<EView> ChangeView;
    }
}
