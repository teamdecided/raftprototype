﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using TeamDecided.RaftConsensus.Common.Logging;
using TeamDecided.RaftPrototype.Enums;

namespace TeamDecided.RaftPrototype.Interfaces
{
    interface INodeView : IView
    {
        string NodeName { get; set; }
        EServerState ServerState { get; set; }
        string ServerStatus { get; set; }

        string AppendKey { get; /*set;*/ }
        string AppendValue { get; /*set;*/ }
        void ResetAppendEntry();

        int TimeoutValue { get; set; }

        bool EnableStartButton { get; set; }
        bool EnableStopButton { get; set; }
        bool EnableAppendEntry { get; set; }

        BindingList<Tuple<string, string>> Log { get; set; }
        void AddLogEntry(Tuple<string, string> logEntry);
        void ClearLog();

        ERaftLogType [] DebugLevels { set; }
        int DebugLevel { get; set; }
        void AppendDebugLog(string line);

        DialogResult DisplayMessage(string title, string message);

        event EventHandler Stop;
        event EventHandler Start;
        event EventHandler AppendEntry;
        event EventHandler DebugLogLevelChange;
    }
}
