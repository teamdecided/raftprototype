﻿using System;
using System.Collections.Generic;
using TeamDecided.RaftPrototype.Models;

namespace TeamDecided.RaftPrototype.Interfaces
{
    public interface INodeSelector : IView
    {
        List<RaftNodeObject> RaftNodeObjects { set; }
        string Node { get; }
        string IPAddress { get; set; }
        string Port { get; set; }
        bool EnableStart { get; set; }
        string FileName { get; }
        bool DeletePersistentStorage { get; }
        string OpenConfigurationFileDialogue();
        bool EnablePersistentStorageRemoval { set; }
        event EventHandler ChangeNode;
        event EventHandler LoadConfigurationFile;
        event EventHandler StartNode;
    }
}
