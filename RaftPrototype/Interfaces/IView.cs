﻿using System;

namespace TeamDecided.RaftPrototype.Interfaces
{
    public interface IView
    {
        event EventHandler Quit;
    }
}
