﻿using System;

namespace TeamDecided.RaftPrototype.Interfaces
{
    public interface IStarterView : IView
    {
        event EventHandler CreateConfig;
        event EventHandler NodeSelector;
        event EventHandler NodeStart;
    }
}
