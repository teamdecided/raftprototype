﻿using System;
using System.Collections.Generic;
using TeamDecided.RaftPrototype.Models;

namespace TeamDecided.RaftPrototype.Interfaces
{
    public interface ICreateView : IView
    {
        string ClusterName { get; set; } 
        bool UseNetworkEncryption { get; set; }
        string Password { get; set; }
        int RetryAttempts { get; set; }
        bool UsePersistentStorage { get; set; }
        int NumberOfNodes { get; set; }
        string SetRetryTime { get; set; }
        List<RaftNodeObject> RaftNodeObjects { get; set; }
        string FileName { get; }

        void DisplayMessage(string title, string message);
        void ChangePasswordState();

        event EventHandler ChangeNetworkEncryptionState;
        event EventHandler CalculateRetryTime;
        event EventHandler ChangeClusterSize;
        event EventHandler<NodeObjectEventArgs> ValidateNodeObjects;
        event EventHandler ValidateClusterName;
        event EventHandler BuildCluster;
    }
}
