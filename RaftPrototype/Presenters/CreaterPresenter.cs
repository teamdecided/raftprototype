﻿using System;
using System.Windows.Forms;
using TeamDecided.RaftPrototype.Enums;
using TeamDecided.RaftPrototype.Interfaces;
using TeamDecided.RaftPrototype.Models;
using TeamDecided.RaftPrototype.Views;

namespace TeamDecided.RaftPrototype.Presenters
{
    public class CreaterPresenter : Presenter
    {
        private RaftConfigurationModel _model;
        private readonly ICreateView _createView;

        public static CreaterPresenter Instance
        {
            get
            {
                if ( _instance == null || _instance.GetType() != typeof(CreaterPresenter))
                {
                    _instance = new CreaterPresenter();
                }
                return (CreaterPresenter) _instance;
            }
        }
        private CreaterPresenter()
        {
            _model = RaftConfigurationModel.DefaultRaftConfigurationModel();

            _view = new RaftCreaterView();
            _createView = (ICreateView)_view;
            ConnectEvents();
        }
        private void ConnectEvents()
        {
            _createView.ChangeNetworkEncryptionState += UseNetworkEncryption_Checked;
            _createView.CalculateRetryTime += UpdateRetryTime;
            _createView.ValidateNodeObjects += ValidateNodeObjects;
            _createView.ChangeClusterSize += ChangeClusterSize;
            _createView.ValidateClusterName += ClusterNameValidate;
            _createView.Quit += View_Quit;
            _createView.BuildCluster += BuildCluster;
        }

        public override event EventHandler<EView> ChangeView;
        public override Form GetView()
        {
            UpdateView();
            return base.GetView();
        }
        public override IPresent GetInstance()
        {
            return Instance;
        }

        private void BuildCluster(object sender, EventArgs e)
        {
            UpdateModel();
            if (_createView.UsePersistentStorage)
            {
                _model.SetPersistentStorageLocation();
            }

            _model.CreateRaftConfigurationFile(_createView.FileName);
            raftConfigurationModel = _model;
            ChangeView?.Invoke(this, EView.NODEVIEW);
        }
        private void UpdateRetryTime(object sender, EventArgs e)
        {
            _model.RetryAttempt = _createView.RetryAttempts;
            _createView.SetRetryTime = string.Format("Retry Time: {0}s", _createView.RetryAttempts * 5);
        }
        private void UseNetworkEncryption_Checked(object sender, EventArgs e)
        {
            _createView.ChangePasswordState();
        }
        private void ValidateNodeObjects(object sender, NodeObjectEventArgs e)
        {
            try
            {
                if (e.Field.Equals(ENodeField.IP))
                {
                    _model.Nodes[e.Index].IP = e.FieldValue;
                }
                else if (e.Field.Equals(ENodeField.PORT))
                {
                    _model.Nodes[e.Index].Port = e.FieldValue;
                }
            }
            catch (FormatException fe)
            {
                _createView.DisplayMessage("IP Format Error", fe.Message);
            }
            catch  (ArgumentOutOfRangeException ae)
            {
                _createView.DisplayMessage("Port Error", ae.Message);
            }
        }
        private void ChangeClusterSize(object sender, EventArgs e)
        {
            _model.SetNumberOfNodes(_createView.NumberOfNodes);
            _createView.RaftNodeObjects = _model.Nodes;
        }
        private void ClusterNameValidate(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(_createView.ClusterName))
            {
                _createView.DisplayMessage("Error", "Cluster name must not be blank");
                _createView.ClusterName = _model.ClusterName;
            }
            else
            {
                _model.ClusterName = _createView.ClusterName;
            }
        }
        private void View_Quit(object sender, EventArgs e)
        {
            ChangeView?.Invoke(this, EView.QUIT);
        }

        private void UpdateView()
        {
            _createView.ClusterName = _model.ClusterName;
            _createView.UseNetworkEncryption = _model.UseNetworkEncryption;
            if (_model.UseNetworkEncryption)
            {
                _createView.Password = _model.EncryptionPassword;
            }
            _createView.RetryAttempts = _model.RetryAttempt;
            _createView.UsePersistentStorage = _model.UsePersistentStorage;
            _createView.NumberOfNodes = _model.NumberOfNodes;
            _createView.RaftNodeObjects = _model.Nodes;
        }
        private void UpdateModel()
        {
            _model.ClusterName= _createView.ClusterName;
            _model.UseNetworkEncryption = _createView.UseNetworkEncryption;
            _model.EncryptionPassword = _createView.Password;
            _model.RetryAttempt = _createView.RetryAttempts;
            _model.UsePersistentStorage = _createView.UsePersistentStorage;
        }
    }
}
