﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Windows.Forms;
using TeamDecided.RaftConsensus.Common.Logging;
using TeamDecided.RaftConsensus.Consensus;
using TeamDecided.RaftConsensus.Consensus.Enums;
using TeamDecided.RaftConsensus.Consensus.Interfaces;
using TeamDecided.RaftPrototype.Enums;
using TeamDecided.RaftPrototype.Interfaces;
using TeamDecided.RaftPrototype.Views;

namespace TeamDecided.RaftPrototype.Presenters
{
    public class NodePresenter : Presenter
    {
        private IConsensus<string, string> _node;
        private readonly BindingList<Tuple<string, string>> _log;
        private bool _isStopped;
        private readonly INodeView _nodeView;

        public override event EventHandler<EView> ChangeView;
        public override IPresent GetInstance() => Instance;

        public static NodePresenter Instance
        {
            get
            {
                if (_instance == null || _instance.GetType() != typeof(NodePresenter))
                {
                    _instance = new NodePresenter();
                }
                return (NodePresenter) _instance;
            }
        }
        private NodePresenter()
        {
            _view = new RaftNodeView();
            _nodeView = (INodeView) _view;
            _log = new BindingList<Tuple<string, string>>();

            ConnectEvents();

            SetupLogging();
            SetViewDefaults();

            Task task = new TaskFactory().StartNew(test =>
            {
                StartNode();
            }, TaskCreationOptions.None);
        }
        private void ConnectEvents()
        {
            _nodeView.Start += StartNode;
            _nodeView.Stop += StopNode;
            _nodeView.AppendEntry += AppendEntry;
            _nodeView.DebugLogLevelChange += DebugLogLevelChange;
            _nodeView.Quit += View_Quit;
        }

        private void HandleNewCommitEntry(object sender, Tuple<string, string> e)
        {
            _nodeView.AddLogEntry(e);
            UpdateView();
        }
        private void HandleUASStart(object sender, EventArgs e)
        {
            Debug.Print("HandleUASStart event");
            UpdateView();
        }
        private void HandleUASStop(object sender, EStopUasReason e)
        {
            Debug.Print("HandleUASStop event {0}", e.ToString());
            UpdateView();
        }
        private void HandleInfoLogUpdate(object sender, Tuple<ERaftLogType, string> e)
        {
            if (CheckLogEntry(e.Item2))
            {
                _nodeView.AppendDebugLog(e.Item2);
                SetNodeStatus(e.Item2);
            }
        }

        private void CreateNode()
        {
            //Instantiate node
            _node = new RaftConsensus<string, string>(NodeObject.Name, int.Parse(NodeObject.Port));

            if (raftConfigurationModel.UsePersistentStorage)
            {
                string curPath = raftConfigurationModel.GetPersistentStoragePath();
                if (!Directory.Exists(curPath))  { Directory.CreateDirectory(curPath); }
                string fileName = string.Format("{0}.db", NodeObject.Name);
                string filePath = Path.Combine(curPath, fileName);
                _node.EnablePersistentStorage(filePath);
            }
            //Add peer to the node
            AddPeers();

            //Subscribe to the node UAS start/stop event
            _node.OnStopUAS += HandleUASStop;
            _node.OnStartUAS += HandleUASStart;
            _node.OnNewCommitedEntry += HandleNewCommitEntry;
        }
        private void SetupLogging()
        {
            RaftLogging.Instance.LogLevel = raftConfigurationModel.LogLevel;
            RaftLogging.Instance.NamedPipeName = string.Format("RaftConsensus: {0}", NodeObject.Name);
            RaftLogging.Instance.WriteToNamedPipe = true;
            RaftLogging.Instance.WriteToEvent = true;
            RaftLogging.Instance.OnNewLogEntry += HandleInfoLogUpdate;
        }

        private void StartNode(object sender, EventArgs e)
        {
            _nodeView.EnableStartButton = false;
            _nodeView.EnableStopButton = true;
            _isStopped = false;
            //_node.Dispose();
            _nodeView.Log.Clear();

            //run the configuration setup on background thread stop GUI from blocking
            Task task = new TaskFactory().StartNew(test =>
            {
                StartNode();
            }, TaskCreationOptions.None);
        }
        private void StopNode(object sender, EventArgs e)
        {
            _isStopped = true;
            _node.Dispose();
            _log.Clear();
            UpdateView();
        }
        private void AppendEntry(object sender, EventArgs e)
        {
            string appendkey = _nodeView.AppendKey;
            string appendValue = _nodeView.AppendValue;
            _node.AppendEntry(appendkey, appendValue);
        }
        private void DebugLogLevelChange(object sender, EventArgs e)
        {
            Debug.Print(((ERaftLogType)_nodeView.DebugLevel).ToString());
            RaftLogging.Instance.LogLevel = (ERaftLogType)_nodeView.DebugLevel;
        }
        private void View_Quit(object sender, EventArgs e)
        {
            RaftLogging.Instance.OnNewLogEntry -= HandleInfoLogUpdate;
            _node.Dispose();

            ChangeView?.Invoke(this, EView.QUIT);
        }

        private void SetViewDefaults()
        {
            _nodeView.NodeName = NodeObject.Name;
            _nodeView.DebugLevels = GetLogLevels();
            _nodeView.DebugLevel = (int) raftConfigurationModel.LogLevel;
            _nodeView.Log = _log;

            UpdateView();
        }
        private ERaftLogType[] GetLogLevels()
        {
            ERaftLogType[] logLevels = Enum.GetValues(typeof(ERaftLogType)).Cast<ERaftLogType>().ToArray();

            string[] logLevelStrings = new string[logLevels.Length];
            for (int i = 0; i < logLevels.Length; i++)
            {
                string temp = logLevels[i].ToString().ToLower();
                logLevelStrings[i] = (temp[0] + "").ToUpper() + temp.Substring(1);
            }

            return logLevels;
        }
        private bool CheckLogEntry(string logEntryLine)
        {
            // this int represents the first character of the node name in the event log entry
            int servername_start_char = 14;

            if (logEntryLine.IndexOf(NodeObject.Name, StringComparison.Ordinal) == servername_start_char)
            {
                return true;
            }
            return false;
        }
        private void SetNodeStatus(string logentry)
        {
            int state_string = 28; //magic number!!!
            string str1 = logentry.Substring(state_string, logentry.IndexOf(')') - state_string);//read log entry to get the status == 'FOLLOWER"

            if (_nodeView.ServerStatus != str1)
            {
                _nodeView.ServerStatus = str1;
            }
        }
        private void StartNode()
        {
            try
            {
                while (true)
                {
                    //Instantiate node and set up peer information
                    //subscribe to RaftLogging Log Info event
                    CreateNode();

                    //call the leader to join cluster
                    Task<EJoinClusterResponse> joinTask = raftConfigurationModel.UseNetworkEncryption ? StartNodeWithEncryption() : StartNodeWithOutEncryption();

                    joinTask.Wait();

                    //check the result of the attempt to join the cluster
                    EJoinClusterResponse result = joinTask.Result;
                    if (result == EJoinClusterResponse.Accept)
                    {
                        break;
                    }
                    if (!_isStopped && _nodeView.DisplayMessage("Error", "Failed to join cluster, do you want to retry?") == DialogResult.Retry)
                    {
                        _nodeView.EnableStartButton = false;
                        _nodeView.EnableStopButton = true;
                    }
                    else
                    {
                        _nodeView.EnableStartButton = true;
                        _nodeView.EnableStopButton = false;
                        return;
                    }

                    _node.Dispose();
                    _nodeView.ClearLog();
                }
                CheckLogStatus();
                UpdateView();
            }
            catch (Exception e)
            {
                _nodeView.DisplayMessage("Exception Thrown", e.ToString());
                Debug.Print("{0}\n{1}", NodeObject.Name,e);
            }
        }
        private Task<EJoinClusterResponse> StartNodeWithEncryption()
        {
            Task<EJoinClusterResponse> joinTask = _node.JoinCluster(
                raftConfigurationModel.ClusterName,
                raftConfigurationModel.EncryptionPassword,
                raftConfigurationModel.NumberOfNodes,
                raftConfigurationModel.RetryAttempt
            );

            return joinTask;
        }
        private Task<EJoinClusterResponse> StartNodeWithOutEncryption()
        {
            Task<EJoinClusterResponse> joinTask = _node.JoinCluster(
                raftConfigurationModel.ClusterName,
                raftConfigurationModel.NumberOfNodes,
                raftConfigurationModel.RetryAttempt
            );

            return joinTask;
        }
        private void AddPeers()
        {
            for (int i = 0; i < raftConfigurationModel.NumberOfNodes; i++)
            {
                //Add the list of nodes into the PeerList
                if (raftConfigurationModel.Nodes[i] == NodeObject)
                {
                    continue;
                }
                IPEndPoint ipEndpoint = new IPEndPoint(IPAddress.Parse(raftConfigurationModel.Nodes[i].IP), int.Parse(raftConfigurationModel.Nodes[i].Port));
                _node.ManualAddPeer(raftConfigurationModel.Nodes[i].Name, ipEndpoint);
            }
        }
        private void UpdateView()
        {
            try
            {
                if (_node != null && _node.IsUASRunning())
                {
                    _nodeView.ServerState = EServerState.ACTIVE;
                    _nodeView.EnableAppendEntry = true;
                    _nodeView.EnableStartButton = false;
                    _nodeView.EnableStopButton = true;
                }
                else
                {
                    if (_isStopped)
                    {
                        _nodeView.ServerState = EServerState.OFFLINE;
                        _nodeView.ServerStatus = "Stopped";
                        _nodeView.EnableStartButton = true;
                        _nodeView.EnableStopButton = false;
                    }
                    else
                    {
                        _nodeView.ServerState = EServerState.INACTIVE;
                        _nodeView.EnableStartButton = false;
                        _nodeView.EnableStopButton = true;
                    }
                    _nodeView.EnableAppendEntry = false;
                }
                _nodeView.ResetAppendEntry();
            }
            catch
            {
                Debug.Print("There was a problem updating the form, probably closing the form.");
            }
        }

        private void CheckLogStatus()
        {
            int count = _node.NumberOfCommits();
            if (count == 0) return;
            for (int i = 0; i < count; i++)
            {
                Tuple<string, string> entry = _node.GetEntryByCommitIndex(i);
                _nodeView.AddLogEntry(entry);
            }
        }
    }
}
