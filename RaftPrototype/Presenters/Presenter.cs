﻿using System;
using System.Windows.Forms;
using TeamDecided.RaftPrototype.Enums;
using TeamDecided.RaftPrototype.Interfaces;
using TeamDecided.RaftPrototype.Models;

namespace TeamDecided.RaftPrototype.Presenters
{
    public abstract class Presenter : IPresent
    {
        protected static Presenter _instance;
        private Form view => (Form) _view;
        protected IView _view;
        protected static RaftNodeObject NodeObject { get; set; }
        protected static RaftConfigurationModel raftConfigurationModel;

        public abstract event EventHandler<EView> ChangeView;

        public abstract IPresent GetInstance();

        public void Close()
        {
            view.Close();
        }
        public virtual Form GetView()
        {
            return view;
        }
        public virtual void Show()
        {
            view.Show();
            view.BringToFront();
        }

        public RaftConfigurationModel GetModel()
        {
            return raftConfigurationModel;
        }
    }
}
