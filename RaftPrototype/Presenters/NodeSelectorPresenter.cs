﻿using System;
using System.Diagnostics;
using System.IO;
using TeamDecided.RaftPrototype.Enums;
using TeamDecided.RaftPrototype.Interfaces;
using TeamDecided.RaftPrototype.Views;
using TeamDecided.RaftPrototype.Models;

namespace TeamDecided.RaftPrototype.Presenters
{
    public class NodeSelectorPresenter : Presenter
    {
        private readonly INodeSelector _nodeSelectorView;

        public static NodeSelectorPresenter Instance
        {
            get
            {
                if (_instance == null || _instance.GetType() != typeof(NodeSelectorPresenter))
                {
                    _instance = new NodeSelectorPresenter();
                }
                return (NodeSelectorPresenter) _instance;
            }
        }
        private NodeSelectorPresenter()
        {
            _view = new RaftNodeSelectorView();

            _nodeSelectorView = (INodeSelector)_view;
            ConnectEvents();
            UpdateView();
        }
        private void ConnectEvents()
        {
            _nodeSelectorView.ChangeNode += ChangeNode;
            _nodeSelectorView.LoadConfigurationFile += LoadConfigurationFile;
            _nodeSelectorView.StartNode += StartNode;
            _nodeSelectorView.Quit += View_Quit;
        }

        private void StartNode(object sender, EventArgs e)
        {
            if (_nodeSelectorView.DeletePersistentStorage)
            {
                DeletePersistentStorage();
            }

            ChangeView?.Invoke(sender, EView.STARTNODE);
        }

        public override event EventHandler<EView> ChangeView;
        public override IPresent GetInstance()
        {
            return Instance;
        }
        public void Show(bool hasCreatedConfguration = false)
        {
            _nodeSelectorView.EnableStart = hasCreatedConfguration;
            base.Show();
            if (raftConfigurationModel == null) LoadConfigurationFile(this, null);
        }

        private void LoadConfigurationFile(object sender, EventArgs e)
        {
            if (_nodeSelectorView.OpenConfigurationFileDialogue() != null)
                raftConfigurationModel = new RaftConfigurationModel(_nodeSelectorView.FileName);

            UpdateView();
            Debug.Print("Loading Configuration File");
        }
        private void ChangeNode(object sender, EventArgs e)
        {
            NodeObject = raftConfigurationModel.Nodes.Find(x => x.Name == _nodeSelectorView.Node);
            _nodeSelectorView.IPAddress = NodeObject.IP;
            _nodeSelectorView.Port = NodeObject.Port;
        }
        private void View_Quit(object sender, EventArgs e)
        {
            ChangeView?.Invoke(this, EView.QUIT);
        }

        private void DeletePersistentStorage()
        {
            string curPath = raftConfigurationModel.GetPersistentStoragePath();
            if (string.IsNullOrWhiteSpace(curPath)) return;

            string fileName = string.Format("{0}.db", NodeObject.Name);
            string filePath = Path.Combine(curPath, fileName);
            File.Delete(filePath);
        }

        private void UpdateView()
        {
            if (raftConfigurationModel != null)
            {
                _nodeSelectorView.EnablePersistentStorageRemoval = raftConfigurationModel.UsePersistentStorage;
            }

            _nodeSelectorView.RaftNodeObjects = raftConfigurationModel?.Nodes;
        }
    }
}
