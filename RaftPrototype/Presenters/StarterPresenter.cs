﻿using System;
using TeamDecided.RaftPrototype.Enums;
using TeamDecided.RaftPrototype.Interfaces;
using TeamDecided.RaftPrototype.Models;
using TeamDecided.RaftPrototype.Views;

namespace TeamDecided.RaftPrototype.Presenters
{
    public class StarterPresenter : Presenter
    {
        public static StarterPresenter Instance
        {
            get
            {
                if (_instance == null || _instance.GetType() != typeof(StarterPresenter))
                {
                    _instance = new StarterPresenter();
                }
                return (StarterPresenter) _instance;
            }
        }
        private StarterPresenter()
        {
            _view = new RaftStarterView();
            ConnectEvents(_view as IStarterView);
        }
        private void ConnectEvents(IStarterView view)
        {
            view.CreateConfig += View_CreateConfig;
            view.NodeSelector += View_NodeSelector;
            view.NodeStart += View_NodeStart;
            view.Quit += View_Quit;
        }

        public override event EventHandler<EView> ChangeView;
        public override IPresent GetInstance()
        {
            return Instance;
        }

        private void View_NodeSelector(object sender, EventArgs e)
        {
            ChangeView?.Invoke(this, EView.NODEVIEW);
        }
        private void View_CreateConfig(object sender, EventArgs e)
        {
            ChangeView?.Invoke(this, EView.CREATERVIEW);
        }
        private void View_NodeStart(object sender, EventArgs e)
        {
            raftConfigurationModel = RaftConfigurationModel.DefaultRaftConfigurationModel();
            NodeObject = raftConfigurationModel.Nodes[0];
            ChangeView?.Invoke(this, EView.STARTNODE);
        }
        private void View_Quit(object sender, EventArgs e)
        {
            ChangeView?.Invoke(this, EView.QUIT);
        }
    }
}
