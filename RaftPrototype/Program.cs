﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using TeamDecided.RaftConsensus.Common.Logging;
using TeamDecided.RaftPrototype.Controllers;

namespace TeamDecided.RaftPrototype
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            try
            {
                RaftLogging.Instance.EnableBuffer(50);

                Controller controller = Controller.Instance;
                var form = controller.GetView();
                Application.Run();
            }
            catch (Exception e)
            {
                Debug.Print(e.ToString());
            }
            finally
            {
                RaftLogging.Instance.FlushBuffer();
            }
        }
    }
}
