﻿using System;
using System.Windows.Forms;
using TeamDecided.RaftPrototype.Interfaces;

namespace TeamDecided.RaftPrototype.Views
{
    public class BaseForm : Form, IView
    {
        public virtual event EventHandler Quit;

        protected override void WndProc(ref Message message)
        {
            //https://bytes.com/topic/c-sharp/answers/933172-detecting-x-button-click-window-winforms
            int SC_CLOSE = 61536; //https://docs.microsoft.com/en-gb/windows/desktop/winmsg/wm-close
            int WM_SYSCOMMAND = 274;//https://docs.microsoft.com/en-us/dotnet/api/system.windows.forms.message.msg?view=netframework-4.7.2
            if (message.Msg == WM_SYSCOMMAND && message.WParam.ToInt32() == SC_CLOSE)
            {
                Quit?.Invoke(this, null);
            }

            base.WndProc(ref message);
        }

        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BaseForm));
            this.SuspendLayout();
            // 
            // BaseForm
            // 
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "BaseForm";
            this.ResumeLayout(false);

        }
    }
}
