﻿namespace TeamDecided.RaftPrototype.Views
{
    partial class RaftCreaterView
    {
        //private System.Drawing.Font _font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RaftCreaterView));
            this.label1 = new System.Windows.Forms.Label();
            this._numberOfNode = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this._clusterName = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this._clusterPasswd = new System.Windows.Forms.TextBox();
            this._useNetworkEncryption = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.lClusterPasswd = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this._usePersistentStorage = new System.Windows.Forms.CheckBox();
            this._retryAttempts = new System.Windows.Forms.NumericUpDown();
            this._retryTimerLabel = new System.Windows.Forms.Label();
            this._warningNodesNumber = new System.Windows.Forms.Label();
            this._nodeObjectsDataView = new System.Windows.Forms.DataGridView();
            this.gbClusterInfo = new System.Windows.Forms.GroupBox();
            this.gbTitle = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this._build = new System.Windows.Forms.Button();
            this._saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this._numberOfNode)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._retryAttempts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._nodeObjectsDataView)).BeginInit();
            this.gbClusterInfo.SuspendLayout();
            this.gbTitle.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 125);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Number of Nodes:";
            // 
            // _numberOfNode
            // 
            this._numberOfNode.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this._numberOfNode.Location = new System.Drawing.Point(141, 121);
            this._numberOfNode.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this._numberOfNode.Minimum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this._numberOfNode.Name = "_numberOfNode";
            this._numberOfNode.Size = new System.Drawing.Size(97, 20);
            this._numberOfNode.TabIndex = 6;
            this._numberOfNode.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this._numberOfNode.ValueChanged += new System.EventHandler(this.NumberOfNode_ValueChanged);
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Cluster Name:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _clusterName
            // 
            this.tableLayoutPanel1.SetColumnSpan(this._clusterName, 2);
            this._clusterName.Dock = System.Windows.Forms.DockStyle.Top;
            this._clusterName.Location = new System.Drawing.Point(141, 3);
            this._clusterName.Name = "_clusterName";
            this._clusterName.Size = new System.Drawing.Size(201, 20);
            this._clusterName.TabIndex = 1;
            this._clusterName.Leave += new System.EventHandler(this.ClusterName_Leave);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.Controls.Add(this._clusterName, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this._clusterPasswd, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this._useNetworkEncryption, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lClusterPasswd, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this._numberOfNode, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.label8, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this._usePersistentStorage, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this._retryAttempts, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this._retryTimerLabel, 2, 6);
            this.tableLayoutPanel1.Controls.Add(this._warningNodesNumber, 2, 9);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(6, 19);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 10;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(345, 145);
            this.tableLayoutPanel1.TabIndex = 10;
            // 
            // _clusterPasswd
            // 
            this.tableLayoutPanel1.SetColumnSpan(this._clusterPasswd, 2);
            this._clusterPasswd.Dock = System.Windows.Forms.DockStyle.Top;
            this._clusterPasswd.Location = new System.Drawing.Point(141, 49);
            this._clusterPasswd.Name = "_clusterPasswd";
            this._clusterPasswd.Size = new System.Drawing.Size(201, 20);
            this._clusterPasswd.TabIndex = 3;
            // 
            // _useNetworkEncryption
            // 
            this._useNetworkEncryption.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this._useNetworkEncryption.AutoSize = true;
            this._useNetworkEncryption.Location = new System.Drawing.Point(141, 29);
            this._useNetworkEncryption.Name = "_useNetworkEncryption";
            this._useNetworkEncryption.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this._useNetworkEncryption.Size = new System.Drawing.Size(97, 14);
            this._useNetworkEncryption.TabIndex = 2;
            this._useNetworkEncryption.UseVisualStyleBackColor = true;
            this._useNetworkEncryption.CheckedChanged += new System.EventHandler(this.UseNetworkEncryption_CheckedChanged);
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 29);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(125, 13);
            this.label7.TabIndex = 19;
            this.label7.Text = "Use Network Encryption:";
            // 
            // lClusterPasswd
            // 
            this.lClusterPasswd.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lClusterPasswd.AutoSize = true;
            this.lClusterPasswd.Location = new System.Drawing.Point(3, 52);
            this.lClusterPasswd.Name = "lClusterPasswd";
            this.lClusterPasswd.Size = new System.Drawing.Size(128, 13);
            this.lClusterPasswd.TabIndex = 11;
            this.lClusterPasswd.Text = "Set Encryption Password:";
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 101);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(96, 13);
            this.label8.TabIndex = 21;
            this.label8.Text = "Persistent Storage:";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 78);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(132, 13);
            this.label3.TabIndex = 23;
            this.label3.Text = "Join Retry Attempts";
            // 
            // _usePersistentStorage
            // 
            this._usePersistentStorage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this._usePersistentStorage.AutoSize = true;
            this._usePersistentStorage.Location = new System.Drawing.Point(141, 101);
            this._usePersistentStorage.Name = "_usePersistentStorage";
            this._usePersistentStorage.Size = new System.Drawing.Size(97, 14);
            this._usePersistentStorage.TabIndex = 5;
            this._usePersistentStorage.UseVisualStyleBackColor = true;
            // 
            // _retryAttempts
            // 
            this._retryAttempts.Location = new System.Drawing.Point(141, 75);
            this._retryAttempts.Maximum = new decimal(new int[] {
            24,
            0,
            0,
            0});
            this._retryAttempts.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this._retryAttempts.Name = "_retryAttempts";
            this._retryAttempts.Size = new System.Drawing.Size(97, 20);
            this._retryAttempts.TabIndex = 4;
            this._retryAttempts.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this._retryAttempts.ValueChanged += new System.EventHandler(this.RetryAttempts_ValueChanged);
            // 
            // _retryTimerLabel
            // 
            this._retryTimerLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this._retryTimerLabel.AutoSize = true;
            this._retryTimerLabel.Location = new System.Drawing.Point(244, 78);
            this._retryTimerLabel.Name = "_retryTimerLabel";
            this._retryTimerLabel.Size = new System.Drawing.Size(98, 13);
            this._retryTimerLabel.TabIndex = 25;
            this._retryTimerLabel.Text = "Retry Time: 5s";
            // 
            // _warningNodesNumber
            // 
            this._warningNodesNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this._warningNodesNumber.AutoSize = true;
            this._warningNodesNumber.Location = new System.Drawing.Point(293, 118);
            this._warningNodesNumber.Name = "_warningNodesNumber";
            this._warningNodesNumber.Size = new System.Drawing.Size(0, 27);
            this._warningNodesNumber.TabIndex = 10;
            this._warningNodesNumber.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _nodeObjectsDataView
            // 
            this._nodeObjectsDataView.AllowDrop = true;
            this._nodeObjectsDataView.AllowUserToAddRows = false;
            this._nodeObjectsDataView.AllowUserToDeleteRows = false;
            this._nodeObjectsDataView.AllowUserToResizeColumns = false;
            this._nodeObjectsDataView.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._nodeObjectsDataView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this._nodeObjectsDataView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._nodeObjectsDataView.DefaultCellStyle = dataGridViewCellStyle2;
            this._nodeObjectsDataView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this._nodeObjectsDataView.Location = new System.Drawing.Point(6, 170);
            this._nodeObjectsDataView.Name = "_nodeObjectsDataView";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._nodeObjectsDataView.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this._nodeObjectsDataView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this._nodeObjectsDataView.Size = new System.Drawing.Size(345, 165);
            this._nodeObjectsDataView.TabIndex = 7;
            this._nodeObjectsDataView.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.NodeObjectsDataView_CellValidating);
            // 
            // gbClusterInfo
            // 
            this.gbClusterInfo.Controls.Add(this._nodeObjectsDataView);
            this.gbClusterInfo.Controls.Add(this.tableLayoutPanel1);
            this.gbClusterInfo.Location = new System.Drawing.Point(12, 100);
            this.gbClusterInfo.Name = "gbClusterInfo";
            this.gbClusterInfo.Size = new System.Drawing.Size(360, 344);
            this.gbClusterInfo.TabIndex = 14;
            this.gbClusterInfo.TabStop = false;
            // 
            // gbTitle
            // 
            this.gbTitle.Controls.Add(this.label6);
            this.gbTitle.Controls.Add(this.label5);
            this.gbTitle.Location = new System.Drawing.Point(12, 12);
            this.gbTitle.Name = "gbTitle";
            this.gbTitle.Size = new System.Drawing.Size(360, 82);
            this.gbTitle.TabIndex = 15;
            this.gbTitle.TabStop = false;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(52, 31);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(248, 24);
            this.label6.TabIndex = 1;
            this.label6.Text = "Cluster Configuration Builder";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(2, 6);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(352, 25);
            this.label5.TabIndex = 0;
            this.label5.Text = "Team Decided - Raft Consensus";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _build
            // 
            this._build.Location = new System.Drawing.Point(288, 450);
            this._build.Name = "_build";
            this._build.Size = new System.Drawing.Size(75, 23);
            this._build.TabIndex = 8;
            this._build.Text = "Build";
            this._build.UseVisualStyleBackColor = true;
            this._build.Click += new System.EventHandler(this.BuildCluster_Click);
            // 
            // _saveFileDialog
            // 
            this._saveFileDialog.FileName = "RaftClusterConfig";
            this._saveFileDialog.Filter = "Raft Cluster Config Files (*.rcc)|*.rcc";
            // 
            // RaftCreaterView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(384, 483);
            this.Controls.Add(this._build);
            this.Controls.Add(this.gbTitle);
            this.Controls.Add(this.gbClusterInfo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "RaftCreaterView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds;
            this.Text = "Raft Consensus - Cluster Configuration Builder";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.QuitApplication);
            ((System.ComponentModel.ISupportInitialize)(this._numberOfNode)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._retryAttempts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._nodeObjectsDataView)).EndInit();
            this.gbClusterInfo.ResumeLayout(false);
            this.gbTitle.ResumeLayout(false);
            this.gbTitle.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown _numberOfNode;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox _clusterName;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label _warningNodesNumber;
        private System.Windows.Forms.Label lClusterPasswd;
        private System.Windows.Forms.TextBox _clusterPasswd;
        private System.Windows.Forms.DataGridView _nodeObjectsDataView;
        private System.Windows.Forms.GroupBox gbClusterInfo;
        private System.Windows.Forms.GroupBox gbTitle;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox _useNetworkEncryption;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox _usePersistentStorage;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown _retryAttempts;
        private System.Windows.Forms.Label _retryTimerLabel;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button _build;
        private System.Windows.Forms.SaveFileDialog _saveFileDialog;
    }
}

