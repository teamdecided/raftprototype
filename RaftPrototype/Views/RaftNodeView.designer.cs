﻿namespace TeamDecided.RaftPrototype.Views
{
    partial class RaftNodeView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RaftNodeView));
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this._serverState = new System.Windows.Forms.Label();
            this._nodeName = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this._stopButton = new System.Windows.Forms.Button();
            this._startButton = new System.Windows.Forms.Button();
            this._log = new System.Windows.Forms.DataGridView();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this._appendEntry = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this._appendKey = new System.Windows.Forms.TextBox();
            this._append = new System.Windows.Forms.Button();
            this._appendValue = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this._startNodeButton = new System.Windows.Forms.Button();
            this._stopNodeButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label5 = new System.Windows.Forms.Label();
            this._debugLevel = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this._serverStatus = new System.Windows.Forms.Label();
            this._debugLog = new System.Windows.Forms.TextBox();
            this.groupBox2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._log)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this._appendEntry.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tableLayoutPanel1);
            this.groupBox2.Location = new System.Drawing.Point(6, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(343, 85);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Consensus Node Detail";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 114F));
            this.tableLayoutPanel1.Controls.Add(this.progressBar1, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this._serverState, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this._nodeName, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(6, 19);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(331, 61);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(220, 33);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(108, 23);
            this.progressBar1.TabIndex = 13;
            this.progressBar1.Visible = false;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Node Name:";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(121, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "User Application Server:";
            // 
            // _serverState
            // 
            this._serverState.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._serverState.AutoSize = true;
            this._serverState.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._serverState.Location = new System.Drawing.Point(130, 39);
            this._serverState.Name = "_serverState";
            this._serverState.Size = new System.Drawing.Size(84, 13);
            this._serverState.TabIndex = 5;
            this._serverState.Text = "Joining Cluster...";
            // 
            // _nodeName
            // 
            this._nodeName.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._nodeName.AutoSize = true;
            this._nodeName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._nodeName.Location = new System.Drawing.Point(130, 8);
            this._nodeName.Name = "_nodeName";
            this._nodeName.Size = new System.Drawing.Size(61, 13);
            this._nodeName.TabIndex = 0;
            this._nodeName.Text = "NodeName";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this._stopButton);
            this.groupBox3.Controls.Add(this._startButton);
            this.groupBox3.Controls.Add(this._log);
            this.groupBox3.Location = new System.Drawing.Point(6, 97);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(343, 351);
            this.groupBox3.TabIndex = 11;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Consensus Log";
            // 
            // _stopButton
            // 
            this._stopButton.Location = new System.Drawing.Point(262, 322);
            this._stopButton.Name = "_stopButton";
            this._stopButton.Size = new System.Drawing.Size(75, 23);
            this._stopButton.TabIndex = 3;
            this._stopButton.Text = "Stop";
            this._stopButton.UseVisualStyleBackColor = true;
            this._stopButton.Click += new System.EventHandler(this.StopButton_Click);
            // 
            // _startButton
            // 
            this._startButton.Location = new System.Drawing.Point(181, 322);
            this._startButton.Name = "_startButton";
            this._startButton.Size = new System.Drawing.Size(75, 23);
            this._startButton.TabIndex = 2;
            this._startButton.Text = "Start";
            this._startButton.UseVisualStyleBackColor = true;
            this._startButton.Click += new System.EventHandler(this.StartButton_Click);
            // 
            // _log
            // 
            this._log.AllowDrop = true;
            this._log.AllowUserToAddRows = false;
            this._log.AllowUserToDeleteRows = false;
            this._log.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._log.Location = new System.Drawing.Point(6, 19);
            this._log.Name = "_log";
            this._log.ReadOnly = true;
            this._log.Size = new System.Drawing.Size(331, 297);
            this._log.TabIndex = 1;
            this._log.TabStop = false;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(2, 2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(363, 566);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this._appendEntry);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(355, 540);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Node Detail";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // _appendEntry
            // 
            this._appendEntry.Controls.Add(this.tableLayoutPanel2);
            this._appendEntry.Enabled = false;
            this._appendEntry.Location = new System.Drawing.Point(2, 454);
            this._appendEntry.Name = "_appendEntry";
            this._appendEntry.Size = new System.Drawing.Size(350, 90);
            this._appendEntry.TabIndex = 12;
            this._appendEntry.TabStop = false;
            this._appendEntry.Text = "Append Entry";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.Controls.Add(this._appendKey, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this._append, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this._appendValue, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label4, 0, 1);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(6, 19);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(334, 61);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // _appendKey
            // 
            this._appendKey.Location = new System.Drawing.Point(46, 3);
            this._appendKey.Name = "_appendKey";
            this._appendKey.Size = new System.Drawing.Size(204, 20);
            this._appendKey.TabIndex = 4;
            // 
            // _append
            // 
            this._append.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this._append.Location = new System.Drawing.Point(256, 19);
            this._append.Name = "_append";
            this.tableLayoutPanel2.SetRowSpan(this._append, 2);
            this._append.Size = new System.Drawing.Size(75, 23);
            this._append.TabIndex = 6;
            this._append.Text = "Append";
            this._append.UseVisualStyleBackColor = true;
            this._append.Click += new System.EventHandler(this.AppendEntry_Click);
            // 
            // _appendValue
            // 
            this._appendValue.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._appendValue.Location = new System.Drawing.Point(46, 33);
            this._appendValue.Name = "_appendValue";
            this._appendValue.Size = new System.Drawing.Size(204, 20);
            this._appendValue.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Key:";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 37);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Value:";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this._startNodeButton);
            this.tabPage2.Controls.Add(this._stopNodeButton);
            this.tabPage2.Controls.Add(this.tableLayoutPanel3);
            this.tabPage2.Controls.Add(this._debugLog);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(355, 540);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Debug Log";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // _startNodeButton
            // 
            this._startNodeButton.Location = new System.Drawing.Point(190, 514);
            this._startNodeButton.Name = "_startNodeButton";
            this._startNodeButton.Size = new System.Drawing.Size(75, 23);
            this._startNodeButton.TabIndex = 6;
            this._startNodeButton.Text = "Start Node";
            this._startNodeButton.UseVisualStyleBackColor = true;
            this._startNodeButton.Click += new System.EventHandler(this.StartButton_Click);
            // 
            // _stopNodeButton
            // 
            this._stopNodeButton.Location = new System.Drawing.Point(271, 514);
            this._stopNodeButton.Name = "_stopNodeButton";
            this._stopNodeButton.Size = new System.Drawing.Size(75, 23);
            this._stopNodeButton.TabIndex = 5;
            this._stopNodeButton.Text = "Stop Node";
            this._stopNodeButton.UseVisualStyleBackColor = true;
            this._stopNodeButton.Click += new System.EventHandler(this.StopButton_Click);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.label5, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this._debugLevel, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.label6, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this._serverStatus, 1, 1);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(6, 6);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.Size = new System.Drawing.Size(343, 50);
            this.tableLayoutPanel3.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 32);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Server Status:";
            // 
            // _debugLevel
            // 
            this._debugLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._debugLevel.FormattingEnabled = true;
            this._debugLevel.Location = new System.Drawing.Point(83, 3);
            this._debugLevel.Name = "_debugLevel";
            this._debugLevel.Size = new System.Drawing.Size(121, 21);
            this._debugLevel.TabIndex = 4;
            this._debugLevel.SelectedIndexChanged += new System.EventHandler(this.DebugLevel_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 7);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Debug Level:";
            // 
            // _serverStatus
            // 
            this._serverStatus.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._serverStatus.AutoSize = true;
            this._serverStatus.Location = new System.Drawing.Point(83, 32);
            this._serverStatus.Name = "_serverStatus";
            this._serverStatus.Size = new System.Drawing.Size(35, 13);
            this._serverStatus.TabIndex = 6;
            this._serverStatus.Text = "label7";
            // 
            // _debugLog
            // 
            this._debugLog.Location = new System.Drawing.Point(6, 58);
            this._debugLog.Multiline = true;
            this._debugLog.Name = "_debugLog";
            this._debugLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._debugLog.Size = new System.Drawing.Size(343, 450);
            this._debugLog.TabIndex = 0;
            this._debugLog.TabStop = false;
            // 
            // RaftNodeView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(367, 575);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "RaftNodeView";
            this.Text = "RaftNode";
            this.groupBox2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._log)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this._appendEntry.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label _serverState;
        private System.Windows.Forms.Label _nodeName;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView _log;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button _stopButton;
        private System.Windows.Forms.Button _startButton;
        private System.Windows.Forms.GroupBox _appendEntry;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TextBox _appendKey;
        private System.Windows.Forms.Button _append;
        private System.Windows.Forms.TextBox _appendValue;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox _debugLog;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button _startNodeButton;
        private System.Windows.Forms.Button _stopNodeButton;
        private System.Windows.Forms.ComboBox _debugLevel;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label _serverStatus;
        private System.Windows.Forms.ProgressBar progressBar1;
    }
}