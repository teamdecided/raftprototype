﻿using System;
using System.Windows.Forms;
using TeamDecided.RaftPrototype.Interfaces;

namespace TeamDecided.RaftPrototype.Views
{
    public partial class RaftStarterView : BaseForm, IStarterView
    {
        public RaftStarterView()
        {
            InitializeComponent();
        }
        public event EventHandler CreateConfig;
        public event EventHandler NodeSelector;
        public event EventHandler NodeStart;

        private void CreateClusterConfig_Click(object sender, EventArgs e)
        {
            CreateConfig?.Invoke(sender, e);
        }
        private void QuitApplication(object sender, FormClosedEventArgs e) {  }
        private void StartNode_Click(object sender, EventArgs e)
        {
            NodeSelector?.Invoke(sender, e);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            NodeStart?.Invoke(sender, e);
        }
    }
}
