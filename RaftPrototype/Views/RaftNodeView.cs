﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Windows.Forms;
using TeamDecided.RaftConsensus.Common.Logging;
using TeamDecided.RaftPrototype.Enums;
using TeamDecided.RaftPrototype.Interfaces;

namespace TeamDecided.RaftPrototype.Views
{
    public partial class RaftNodeView : BaseForm, INodeView
    {
        private const string KeyHeading = "Key";
        private const string ValueHeading = "Value";
        private readonly SynchronizationContext _mainThread;

        private event EventHandler ServerStateChange;
        public event EventHandler Stop;
        public event EventHandler Start;
        public event EventHandler AppendEntry;
        public event EventHandler DebugLogLevelChange;

        public RaftNodeView()
        {
            InitializeComponent();
            _mainThread = SynchronizationContext.Current;

            ServerStateChange += ServerState_Change;
        }

        private void StartButton_Click(object sender, EventArgs e)
        {
            Start?.Invoke(sender, e);
        }
        private void StopButton_Click(object sender, EventArgs e)
        {
            Stop?.Invoke(sender, e);
        }
        private void ServerState_Change(object sender, EventArgs e)
        {
            if (ServerState == EServerState.ACTIVE)
            {
                _serverState.ForeColor = System.Drawing.Color.Green;
            }
            else if (ServerState == EServerState.INACTIVE)
            {
                _serverState.ForeColor = System.Drawing.Color.Orange;
            }
            else if (ServerState == EServerState.OFFLINE)
            {
                _serverState.ForeColor = System.Drawing.Color.Red;
            }
        }
        private void AppendEntry_Click(object sender, EventArgs e)
        {
            AppendEntry?.Invoke(sender, e);
        }
        private void DebugLevel_SelectedIndexChanged(object sender, EventArgs e)
        {
            DebugLogLevelChange?.Invoke(sender, e);
        }

        public string NodeName
        {
            get => _nodeName.Text;
            set => _mainThread.Post( state =>
            {
                _nodeName.Text = value;
            }, null);
        }
        private EServerState __serverState;
        public EServerState ServerState {
            get => __serverState;
            set
            {
                _mainThread.Post(state =>
                {
                    __serverState = value;
                    _serverState.Text = SentenceCaseString(__serverState.ToString());
                    ServerStateChange?.Invoke(this, null);
                }, null);
            }
        }
        public string ServerStatus
        {
            get => _serverStatus.Text;
            set => _mainThread.Post(state =>
            {
                _serverStatus.Text = value; 
            }, null);
        }
        public DialogResult DisplayMessage(string title, string message)
        {
            return MessageBox.Show(message, title, MessageBoxButtons.RetryCancel, MessageBoxIcon.Error);
        }
        public int DebugLevel
        {
            get => _debugLevel.SelectedIndex;
            set => _mainThread.Post(state =>
            {
                _debugLevel.SelectedIndex = value; 
            }, null);
        }

        public string AppendKey => _appendKey.Text;
        public string AppendValue => _appendValue.Text;

        public void ResetAppendEntry()
        {
            _mainThread.Post(state =>
            {
                _appendKey.Text = "";
                _appendValue.Text = "";
            }, null);
        }
        public int TimeoutValue { get; set; }

        public ERaftLogType[] DebugLevels
        {
            set => _debugLevel.DataSource = value;
        }
        public bool EnableStartButton
        {
            get => _startButton.Enabled;
            set
            {
                _mainThread.Post(state =>
                {
                    _startButton.Enabled = value;
                    _startNodeButton.Enabled = value;
                },null);
            }
        }
        public bool EnableStopButton
        {
            get => _stopButton.Enabled;
            set
            {
                _mainThread.Post(state =>
                {
                    _stopButton.Enabled = value;
                    _stopNodeButton.Enabled = value;
                }, null);
            } 
        }
        public bool EnableAppendEntry
        {
            get => _appendEntry.Enabled;
            set => _mainThread.Post( state =>
            {
                _appendEntry.Enabled = value;
            },null);
        }

        private BindingList<Tuple<string, string>> _logData;
        public BindingList<Tuple<string, string>> Log
        {
            get => _logData;
            set
            {
                _logData = value;
                _log.DataSource = _logData;
                FormatDatGrid();
            }
        }
        public void AddLogEntry(Tuple<string, string> logEntry)
        {
            _mainThread.Post(state =>
            {
                _logData.Add(logEntry);
            }, null);
        }

        public void ClearLog()
        {
            _mainThread.Post(state =>
            {
                _logData.Clear();
            }, null);
        }
        private void FormatDatGrid()
        {
            foreach (DataGridViewColumn col in _log.Columns)
            {
                col.ReadOnly = true;
                col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            }

            _log.Columns[0].HeaderText = KeyHeading;
            _log.Columns[1].HeaderText = ValueHeading;
        }

        public void AppendDebugLog(string line)
        {
            _mainThread.Post(state =>
            {
            _debugLog.AppendText(line);
            }, null);
        }
        private string SentenceCaseString(string str)
        {
            string temp = str.ToLower();
            string formatted = (temp[0] + "").ToUpper() + temp.Substring(1);
            return formatted;
        }
    }
}

