﻿using System;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using TeamDecided.RaftConsensus.Consensus;
using TeamDecided.RaftConsensus.Consensus.Interfaces;
using TeamDecided.RaftConsensus.Consensus.Enums;
using TeamDecided.RaftConsensus.Common.Logging;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace RaftPrototype
{
    public partial class RaftNode : Form
    {
        private IConsensus<string, string> _node;
        private SynchronizationContext _mainThread;

        //private readonly List<Tuple<string, string>> _log;
        private readonly BindingList<Tuple<string, string>> _log;
        private static readonly object UpdateWindowLockObject = new object();

        private RaftBootstrapConfig _config;
        public string Servername { get; private set; }
        public string Serverip { get; private set; }
        public int Serverport { get; private set; }
        private ERaftLogType _logLevel;
        private bool _useEncryption;
        private bool _usePersistentStorage;
        public int Index { get; private set; }
        private readonly string _configurationFile;
        //private readonly string _logfile;

        private static readonly Mutex Mutex = new Mutex();
        private bool _onClosing;
        private bool _isStopped;
        private const int MAX_ATTEMPTS = 1;

        private const int xx = 14;

        public RaftNode(int index, string configFile, bool isInstansiated = false)
        {
            //set local attributes
            Index = index;
            _configurationFile = configFile;
            _log = new BindingList<Tuple<string, string>>();
            _isStopped = false;
            _mainThread = SynchronizationContext.Current;
            _onClosing = false;

            if (_mainThread == null)
            {
                _mainThread = new SynchronizationContext();
            } 

            InitializeComponent();
            Initialize();

            if(isInstansiated)
            {
                cbDebugLevel.Enabled = false;
            }
        }

        private void Initialize()
        {
            LoadConfig();//populate object with info from config file

            Text = string.Format("{0} - {1}", Text, Servername);

            btStart.Enabled = false;
            FormBorderStyle = FormBorderStyle.FixedDialog;
            logDataGrid.DataSource = _log;

            SetUpLogLevelDropDownList();

            SetupLogging();

            Task task = new TaskFactory().StartNew(test =>
            {
                StartNode();
            }, TaskCreationOptions.None);
        }

        private void SetUpLogLevelDropDownList()
        {
            ERaftLogType[] logLevels = Enum.GetValues(typeof(ERaftLogType)).Cast<ERaftLogType>().ToArray();

            string[] logLevelStrings = new string[logLevels.Length];
            for (int i = 0; i < logLevels.Count(); i++)
            {
                string temp = logLevels[i].ToString().ToLower();
                logLevelStrings[i] = (temp[0] + "").ToUpper() + temp.Substring(1);
            }

            cbDebugLevel.DataSource = logLevelStrings;
            cbDebugLevel.SelectedIndex = (int)_logLevel;
        }

        public void LoadConfig()
        {
            string json = File.ReadAllText(_configurationFile);
            _config = JsonConvert.DeserializeObject<RaftBootstrapConfig>(json);
            Servername = _config.nodeNames[Index];
            Serverport = int.Parse(_config.nodePorts[Index]);
            Serverip = _config.nodeIPAddresses[Index];
            _logLevel = _config.logLevel;
            _useEncryption = _config.useEncryption;
            _usePersistentStorage = _config.usePersistentStorage;
        }

        private void StartNode()
        {
            try
            {
                while (true)
                {
                    //Instantiate node and set up peer information
                    //subscribe to RaftLogging Log Info event
                    CreateNode();

                    //call the leader to join cluster
                    Task<EJoinClusterResponse> joinTask = _useEncryption ? StartNodeWithEncryption() : StartNodeWithOutEncryption();

                    joinTask.Wait();

                    //check the result of the attempt to join the cluster
                    EJoinClusterResponse result = joinTask.Result;
                    if (result == EJoinClusterResponse.Accept)
                    {
                        break;
                    }
                    else
                    {
                        _node.Dispose();
                        _log.Clear();
                        if (MessageBox.Show("Failed to join cluster, do you want to retry?", "Error " + Servername, MessageBoxButtons.RetryCancel, MessageBoxIcon.Error) == DialogResult.Retry)
                        {
                            //continue;
                        }
                        else
                        {
                            _mainThread.Send( state =>
                            {
                                lock (UpdateWindowLockObject)
                                {
                                    btStart.Enabled = true;
                                }
                            }, null);
                            return;
                        }
                    }
                }

                //update the main UI
                _mainThread.Send( state =>
                {
                    lock (UpdateWindowLockObject)
                    {
                        UpdateNodeWindow();
                    }
                }, null);
            }
            catch (Exception e)
            {
                MessageBox.Show(Servername + "\n" + e);
            }
        }

        private Task<EJoinClusterResponse> StartNodeWithEncryption()
        {
            Task<EJoinClusterResponse> joinTask = _node.JoinCluster(_config.clusterName,
                _config.clusterPassword,
                _config.maxNodes,
                MAX_ATTEMPTS
            );

            return joinTask;
        }

        private Task<EJoinClusterResponse> StartNodeWithOutEncryption()
        {
            Task<EJoinClusterResponse> joinTask = _node.JoinCluster(_config.clusterName,
                _config.maxNodes,
                MAX_ATTEMPTS
            );

            return joinTask;
        }

        private void CreateNode()
        {
            //Instantiate node
            _node = new RaftConsensus<string, string>(_config.nodeNames[Index], int.Parse(_config.nodePorts[Index]));

            if (_usePersistentStorage)
            {
                string curPath = System.IO.Path.GetDirectoryName(Application.ExecutablePath);
                string fileName = string.Format("{0}.db", _config.nodeNames[Index]);
                string filePath = Path.Combine(curPath, fileName);

                _node.EnablePersistentStorage(filePath);
            }
            //Add peer to the node
            AddPeers(_config, Index);

            //Subscribe to the node UAS start/stop event
            _node.OnStopUAS += HandleUASStop;
            _node.OnStartUAS += HandleUASStart;
            _node.OnNewCommitedEntry += HandleNewCommitEntry;
        }

        private void SetupLogging()
        {
            RaftLogging.Instance.NamedPipeName = string.Format("RaftConsensus{0}", Index);
            //RaftLogging.Instance.WriteToNamedPipe = false;
            RaftLogging.Instance.WriteToEvent = true;
            RaftLogging.Instance.OnNewLogEntry += HandleInfoLogUpdate;
        }

        private void HandleInfoLogUpdate(object sender, Tuple<ERaftLogType, string> e)
        {
            try
            {
                if (Mutex.WaitOne())
                {
                    if (!_onClosing)
                    {
                        _mainThread.Post(state =>
                        {
                            if (CheckLogEntry(e.Item2))
                            {
                                try
                                {
                                    tbLog.AppendText(e.Item2);
                                    SetNodeStatus(e.Item2);
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine("Exception was thrown during async Post() update of GUI\n{0}", ex);
                                }
                            }
                        }, null);
                    }
                }
            }
            finally
            {
                Mutex.ReleaseMutex();
            }
        }

        private void UpdateNodeWindow()
        {
            try
            {
                lbNodeName.Text = Servername;
                if (_node != null && _node.IsUASRunning())
                {
                    lbServerState.Text = "Active";
                    lbServerState.ForeColor = System.Drawing.Color.Green;
                    gbAppendEntry.Enabled = true;
                    btStart.Enabled = false;
                    btStartNode.Enabled = false;
                    btStop.Enabled = true;
                    btStopNode.Enabled = true;
                }
                else
                {
                    if (_isStopped)
                    {
                        lbServerState.Text = "Offline";
                        lbServerState.ForeColor = System.Drawing.Color.Red;
                        btStart.Enabled = true;
                        btStartNode.Enabled = true;
                        btStop.Enabled = false;
                        btStopNode.Enabled = false;
                    }
                    else
                    {
                        lbServerState.Text = "Inactive";
                        lbServerState.ForeColor = System.Drawing.Color.Orange;
                        btStart.Enabled = false;
                        btStartNode.Enabled = false;
                        btStop.Enabled = true;
                        btStopNode.Enabled = true;
                    }

                    gbAppendEntry.Enabled = false;
                }

                tbKey.Clear();
                tbValue.Clear();
                //logDataGrid.DataSource = null;
                //logDataGrid.DataSource = _log;
                if (_log.Count() != 0)
                {
                    logDataGrid.Columns[0].HeaderText = "Key";
                    logDataGrid.Columns[1].HeaderText = "Value";
                }
            }
            catch 
            {
                Console.WriteLine("There was a problem updating the form, probably closing the form.");
            }
        }

        private void HandleUASStart(object sender, EventArgs e)
        {
            _mainThread.Post( state =>
            {
                UpdateNodeWindow();
            }, null);
        }

        private void HandleUASStop(object sender, EStopUasReason e)
        {
            _mainThread.Post( state =>
            {
                UpdateNodeWindow();
            }, null);
        }

        private void HandleNewCommitEntry(object sender, Tuple<string, string> e)
        {
            _mainThread.Post( state =>
            {
                // fix internal binding log 
                _log.Add(e);
                UpdateNodeWindow();
            }, null);
        }

        private void Stop_Click(object sender, EventArgs e)
        {
            _isStopped = true;

            _node.Dispose();
            lock (UpdateWindowLockObject)
            {
                UpdateNodeWindow();
            }
        }

        private void Start_Click(object sender, EventArgs e)
        {
            btStart.Enabled = false;
            _isStopped = false;
            _log.Clear();

            //run the configuration setup on background thread stop GUI from blocking
            Task task = new TaskFactory().StartNew(test =>
            {
                StartNode();
            }, TaskCreationOptions.None);
        }

        private void AppendMessage_Click(object sender, EventArgs e)
        {
            if (tbKey.Text == "" || tbValue.Text == "")
            {
                MessageBox.Show("Key or Value should not be blank\n(not a limitation, it's just dumb, sorry :P)", "Warning - Blank key or value", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            Task<ERaftAppendEntryState> append = _node.AppendEntry(tbKey.Text, tbValue.Text);

            tbKey.Clear();
            tbValue.Clear();
        }

        private void cbDebugLevel_SelectedIndexChanged(object sender, EventArgs e)
        {
            RaftLogging.Instance.LogLevel = ((ERaftLogType)cbDebugLevel.SelectedIndex);
        }

        private bool CheckLogEntry(string logEntryLine)
        {
            // this int represents the first character of the node name in the event log entry
            int servername_start_char = 14;

            if ( logEntryLine.IndexOf(Servername, StringComparison.Ordinal) == servername_start_char)
            {
                return true;
            }
            return false;
        }

        private void SetNodeStatus(string logentry)
        {
            int state_string = 28; //magic number!!!
            string str1 = logentry.Substring(state_string, logentry.IndexOf(')') - state_string);//read log entry to get the status == 'FOLLOWER"

            if (lServerStatus.Text != str1)
            {
                lServerStatus.Text = str1;
            }
        }

        private void AddPeers(RaftBootstrapConfig config, int id)
        {
            for (int i = 0; i < config.maxNodes; i++)
            {
                //Add the list of nodes into the PeerList
                if (i == id)
                {
                    continue;
                }
                IPEndPoint ipEndpoint = new IPEndPoint(IPAddress.Parse(config.nodeIPAddresses[i]), int.Parse(config.nodePorts[i]));
                _node.ManualAddPeer(config.nodeNames[i], ipEndpoint);
            }
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            try
            {
                Mutex.WaitOne();
                _onClosing = true;
                RaftLogging.Instance.OnNewLogEntry -= HandleInfoLogUpdate;
            }
            finally
            {
                Mutex.ReleaseMutex();
            }
            base.OnClosing(e);
        }

        protected override void OnFormClosed(FormClosedEventArgs e)
        {
            _node.Dispose();
            _mainThread = null;
            base.OnFormClosed(e);
        }

        internal class RaftBootstrapConfig
        {
            public string clusterName;
            public bool useEncryption;
            public string clusterPassword;
            public int maxNodes;
            public List<string> nodeNames = new List<string>();
            public List<string> nodeIPAddresses = new List<string>();
            public List<string> nodePorts = new List<string>();
            public ERaftLogType logLevel;
            public bool usePersistentStorage;
            public int RetryAttempt;
        }
    }

}

