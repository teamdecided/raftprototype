﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using TeamDecided.RaftPrototype.Enums;
using TeamDecided.RaftPrototype.Interfaces;
using TeamDecided.RaftPrototype.Models;

namespace TeamDecided.RaftPrototype.Views
{
    public partial class RaftCreaterView : BaseForm, ICreateView
    {
        private const string SaveTitle = "Save Raft Configuration Files";
        private const string SaveFilter = "Raft Cluster Config Files (*.rcc)|*.rcc";
        private const string MinimumNodesMessage = "Minimum 3 nodes";
        private const string MaximumNodesMessage = "Maximum 9 nodes";

        public event EventHandler ChangeNetworkEncryptionState;
        public event EventHandler CalculateRetryTime;
        public event EventHandler<NodeObjectEventArgs> ValidateNodeObjects;
        public event EventHandler ChangeClusterSize;
        public event EventHandler ValidateClusterName;
        public event EventHandler BuildCluster;

        public RaftCreaterView()
        {
            InitializeComponent();
        }

        public string ClusterName
        {
            get => _clusterName.Text;
            set => _clusterName.Text = value;
        }
        public string Password
        {
            get => _clusterPasswd.Text;
            set => _clusterPasswd.Text = value;
        }
        public int NumberOfNodes
        {
            get => (int) _numberOfNode.Value;
            set
            {
                _numberOfNode.Value = value;
                SetNumberOfNodesWarning();
            }
        }
        public int RetryAttempts
        {
            get => (int)_retryAttempts.Value;
            set => _retryAttempts.Value = value;
        }
        public bool UsePersistentStorage
        {
            get => _usePersistentStorage.Checked;
            set => _usePersistentStorage.Checked = value;
        }
        public bool UseNetworkEncryption
        {
            get => _useNetworkEncryption.Checked; 
            set
            {
                _useNetworkEncryption.Checked = value;
                ChangePasswordState();
            }
        }
        public string SetRetryTime
        {
            get => _retryTimerLabel.Text;
            set => _retryTimerLabel.Text = value;
        }
        public List<RaftNodeObject> RaftNodeObjects
        {
            get => GetNodeList();
            set
            {
                _nodeObjectsDataView.DataSource = null;
                _nodeObjectsDataView.DataSource = value;
                _nodeObjectsDataView.Columns[0].ReadOnly = true;
                foreach (DataGridViewColumn col in _nodeObjectsDataView.Columns)
                {
                    col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                }
            }
        }

        public string FileName { get; private set; }

        private void UseNetworkEncryption_CheckedChanged(object sender, EventArgs e)
        {
            ChangeNetworkEncryptionState?.Invoke(sender, e);
        }
        private void RetryAttempts_ValueChanged(object sender, EventArgs e)
        {
            CalculateRetryTime?.Invoke(sender, e);
        }
        private void NodeObjectsDataView_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (e.ColumnIndex == 0) return;
            NodeObjectEventArgs args = new NodeObjectEventArgs()
            {
                Index = e.RowIndex,
                Field = e.ColumnIndex==1?ENodeField.IP:ENodeField.PORT,
                FieldValue = e.FormattedValue.ToString()
            };

            ValidateNodeObjects?.Invoke(sender, args);
            _nodeObjectsDataView.RefreshEdit();
        }
        private void NumberOfNode_ValueChanged(object sender, EventArgs e)
        {
            SetNumberOfNodesWarning();
            ChangeClusterSize?.Invoke(sender, e);
        }
        private void ClusterName_Leave(object sender, EventArgs e)
        {
            ValidateClusterName?.Invoke(sender, e);
        }
        private void QuitApplication(object sender, FormClosedEventArgs e) {  }
        private void BuildCluster_Click(object sender, EventArgs e)
        {
            //IsBuilding = true;
            _saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            _saveFileDialog.Title = SaveTitle;
            _saveFileDialog.CheckPathExists = true;
            _saveFileDialog.DefaultExt = "rcc";
            _saveFileDialog.Filter = SaveFilter;
            _saveFileDialog.FilterIndex = 2;
            _saveFileDialog.RestoreDirectory = true;
            if (_saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                FileName = _saveFileDialog.FileName;
                //CreateRaftConfigurationFile(ConfigFile);
                BuildCluster?.Invoke(sender, e);
            }
        }
        
        public void DisplayMessage(string title, string message)
        {
            MessageBox.Show(message, title);
        }
        public void ChangePasswordState()
        {
            _clusterPasswd.Text = "";
            _clusterPasswd.Enabled = _useNetworkEncryption.Checked;
        }

        /// <summary>
        /// Iterates over the datagrid view and returns a list RaftNodeObjects objects
        /// </summary>
        /// <returns>The contents of the datagrid</returns>
        private List<RaftNodeObject> GetNodeList()
        {
            List<RaftNodeObject> items = new List<RaftNodeObject>();
            foreach (DataGridViewRow dr in _nodeObjectsDataView.Rows)
            {
                RaftNodeObject item = new RaftNodeObject()
                {
                    Name = (string) dr.Cells[0].Value,
                    IP  = (string)dr.Cells[1].Value,
                    Port = (string)dr.Cells[2].Value
                };
                items.Add(item);
            }
            return items;
        }
        /// <summary>
        /// Sets the warning label based on the control's hard wired settings
        /// </summary>
        private void SetNumberOfNodesWarning()
        {
            if (_numberOfNode.Value == 3)
            {
                _warningNodesNumber.Text = MinimumNodesMessage;
            }
            else if (_numberOfNode.Value == 9)
            {
                _warningNodesNumber.Text = MaximumNodesMessage;
            }
            else
            {
                _warningNodesNumber.Text = "";
            }
        }
    }
}