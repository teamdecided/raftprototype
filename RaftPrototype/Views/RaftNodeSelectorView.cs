﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using TeamDecided.RaftPrototype.Interfaces;
using TeamDecided.RaftPrototype.Models;

namespace TeamDecided.RaftPrototype.Views
{
    public partial class RaftNodeSelectorView : BaseForm, INodeSelector
    {
        private const string OpenTitle = "Open Raft Configuration Files";
        private const string OpenFilter = "Raft Cluster Config Files (*.rcc)|*.rcc";

        public RaftNodeSelectorView()
        {
            InitializeComponent();
        }
        public List<RaftNodeObject> RaftNodeObjects
        {
            set => _nodes.DataSource = value;
        }

        public RaftNodeObject NodeObject => (RaftNodeObject)_nodes.SelectedItem;

        public string Node => _nodes.Text;

        public string IPAddress
        {
            get => _ipAddress.Text;
            set => _ipAddress.Text = value;
        }
        public string Port
        {
            get => _port.Text;
            set => _port.Text = value;
        }
        public bool EnableStart
        {
            get => _start.Enabled;
            set => _start.Enabled = value;
        }
        public string FileName
        {
            get;
            private set;
        }
        public bool EnablePersistentStorageRemoval
        {
            set => _deleteStorage.Enabled = value;
        }
        public bool DeletePersistentStorage => _deleteStorage.Checked;

        public event EventHandler ChangeNode;
        public event EventHandler LoadConfigurationFile;
        public event EventHandler StartNode;

        private void QuitApplication(object sender, FormClosedEventArgs e) {  }
        private void Node_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeNode?.Invoke(sender, e);
        }
        private void IPAddress_MouseEnter(object sender, EventArgs e)
        {
            _ipAddress.PasswordChar = '\0';
        }
        private void IPAddress_MouseLeave(object sender, EventArgs e)
        {
            _ipAddress.PasswordChar = '*';
        }
        private void Load_Click(object sender, EventArgs e)
        {
            LoadConfigurationFile?.Invoke(sender, e);
        }
        private void StartNode_Click(object sender, EventArgs e)
        {
            StartNode?.Invoke(sender, e);
        }

        public string OpenConfigurationFileDialogue()
        {
            _openFileDialog = new OpenFileDialog
            {
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
                Title = OpenTitle,
                DefaultExt = "rcc",
                Filter = OpenFilter
            };

            // Show the Dialog.  
            // If the user clicked OK in the dialog and  
            // a .rcc file was selected, open it.
            if (_openFileDialog.ShowDialog() == DialogResult.OK)
            {
                FileName = _openFileDialog.FileName;
                _start.Enabled = true;
                return FileName;
            }

            return null;
        }
    }
}
