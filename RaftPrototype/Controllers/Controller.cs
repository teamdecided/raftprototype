﻿using System.Diagnostics;
using System.Windows.Forms;
using TeamDecided.RaftPrototype.Enums;
using TeamDecided.RaftPrototype.Interfaces;
using TeamDecided.RaftPrototype.Presenters;

namespace TeamDecided.RaftPrototype.Controllers
{
    public class Controller : ApplicationContext
    {
        private Form _currentView;
        private IPresent _currentPresenter;
        private static Controller _instance;
        private bool _hasUsedCreater;

        private Controller()
        {
            SetView(StarterPresenter.Instance);
            _currentPresenter.Show();
        }
        public static Controller Instance => _instance ?? (_instance = new Controller());

        public Form GetView()
        {
            return _currentView;
        }

        private void SetView(IPresent view)
        {
            _currentPresenter?.Close();
            _currentPresenter = null;
            _currentPresenter = view.GetInstance();
            _currentView = _currentPresenter.GetView();
            _currentPresenter.ChangeView += ChangeViewEventHandler;
        }
        private void QuitApplication()
        {
            _currentPresenter = null;
            _currentView = null;
            Application.Exit();
        }
        private void DebugMessage(string message)
        {
            Debug.Print(message);
        }
        private void ChangeViewEventHandler(object sender, EView e)
        {
            switch(e)
            {
                case EView.QUIT:
                    QuitApplication();
                    string message = string.Format("{0} INITIATED {1}", sender, e.ToString());
                    DebugMessage(message);
                    break;
                case EView.STARTERVIEW:
                    DebugMessage(EView.STARTERVIEW.ToString());
                    break;
                case EView.CREATERVIEW:
                    SetView(CreaterPresenter.Instance);
                    _currentPresenter.Show();
                    DebugMessage(EView.CREATERVIEW.ToString());
                    _hasUsedCreater = true;
                    break;
                case EView.NODEVIEW:
                    SetView(NodeSelectorPresenter.Instance);
                    (_currentPresenter as NodeSelectorPresenter)?.Show(_hasUsedCreater);
                    DebugMessage(EView.NODEVIEW.ToString());
                    break;
                case EView.STARTNODE:
                    SetView(NodePresenter.Instance);
                    _currentPresenter.Show();
                    break;
            }
        }
    }
}
