﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using TeamDecided.RaftConsensus.Common.Logging;

namespace TeamDecided.RaftPrototype.Models
{
    public class RaftConfigurationModel
    {
        private const string TEAMDECIDED = "Team Decided";
        private const int DEFAULTNUMBEROFNODES = 3;
        private string BASEDIRECTORY => BaseDirectory();

        public string ClusterName { get; set; }
        public bool UseNetworkEncryption { get; set; }
        public string EncryptionPassword { get; set; }
        public int NumberOfNodes { get; private set; }
        public List<RaftNodeObject> Nodes { get; set; }
        public ERaftLogType LogLevel { get; set; }
        public bool UsePersistentStorage { get; set; }
        public int RetryAttempt { get; set; }

        private string _persistentStoragePath;

        public static RaftConfigurationModel DefaultRaftConfigurationModel()
        {
            RaftConfigurationModel model = new RaftConfigurationModel
            {
                Nodes = new List<RaftNodeObject>(),
                ClusterName = "ClusterName",
                UseNetworkEncryption = false,
                EncryptionPassword = "password123",
                LogLevel = ERaftLogType.Info,
                UsePersistentStorage = false,
                RetryAttempt = 1
            };

            model.SetNumberOfNodes(DEFAULTNUMBEROFNODES);

            return model;
        }
        public RaftConfigurationModel() {  }
        public RaftConfigurationModel(string filename)
        {
            if (File.Exists(filename))
            {
                ReadConfigFile(filename);
            }
        }

        public void SetNumberOfNodes(int numberOfNodes)
        {
            UpdateNodeList(numberOfNodes);
            NumberOfNodes = numberOfNodes;
        }

        public void CreateRaftConfigurationFile(string filename)
        {
            int maxNodes = NumberOfNodes;

            // serialize the config object
            string json = JsonConvert.SerializeObject(this, Formatting.Indented);

            //delete old config and create new file 
            File.Delete(filename);
            File.WriteAllText(filename, json);
        }

        public string GetPersistentStoragePath()
        {
            return _persistentStoragePath ?? (_persistentStoragePath = BASEDIRECTORY);
        }
        public void SetPersistentStorageLocation(string path = null)
        {
            if (path == null)
            {
                _persistentStoragePath = BASEDIRECTORY;
                return;
            }

            if (Directory.Exists(path))
            {
                _persistentStoragePath = path;
            }
        }

        private void ReadConfigFile(string filename)
        {
            string json = File.ReadAllText(filename);
            RaftConfigurationModel fromFile = JsonConvert.DeserializeObject<RaftConfigurationModel>(json);
            PopulateModel(fromFile);
        }
        private void PopulateModel(RaftConfigurationModel source)
        {
            ClusterName = source.ClusterName;
            UseNetworkEncryption = source.UseNetworkEncryption;
            EncryptionPassword = source.EncryptionPassword;
            NumberOfNodes = source.Nodes.Count;
            LogLevel = source.LogLevel;
            UsePersistentStorage = source.UsePersistentStorage;
            RetryAttempt = source.RetryAttempt;
            Nodes = source.Nodes;
        }
        private void UpdateNodeList(int size)
        {
            if (size > NumberOfNodes)
            {
                for (int i = Nodes.Count + 1; i <= size; i++)
                {
                    RaftNodeObject nodeObject = new RaftNodeObject() { Name = "Node" + i, IP = "127.0.0." + i, Port = string.Format("{0}", 5555 + i) };
                    Nodes.Add(nodeObject);
                }
            }
            else if (size < NumberOfNodes)
            {
                int i = 0;
                List<RaftNodeObject> toRemove = new List<RaftNodeObject>();
                foreach (RaftNodeObject r in Nodes)
                {
                    i++;
                    if (i > size)
                    {
                        toRemove.Add(r);
                    }
                }

                foreach (RaftNodeObject obj in toRemove)
                {
                    Nodes.Remove(obj);
                }
            }
            else
            {
                // must be same do nothing
            }
        }
        private string BaseDirectory()
        {
            string rootdirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            string basedirectory = Path.Combine(rootdirectory, TEAMDECIDED);
            return basedirectory;
        }
    }
}
