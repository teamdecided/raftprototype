﻿using System;
using System.Linq;

namespace TeamDecided.RaftPrototype.Models
{
    public class RaftNodeObject
    {
        private const string InvalidPortMessage = "Port number must be greater than 1024 and less than 65535";
        private const string InvalidIPMessage = "IP address must be IPv4 dotted decimal notation";

        public string Name { get; set; }
        public string IP
        {
            get => _ip;
            set
            {
                if (!ValidateIPv4(value))
                {
                    throw new FormatException(InvalidIPMessage);
                }

                _ip = value;
            }
        }


        public string Port
        {
            get => _port;
            set
            {
                if (!ValidatePort(value))
                {
                    throw new ArgumentOutOfRangeException(Port, InvalidPortMessage);
                }

                _port = value;
            }
        }

        private string _ip;
        private string _port;

        private bool ValidatePort(string str)
        {
            return int.TryParse(str, out int num) && (num > 1024 && num < 65535);
        }
        private bool ValidateIPv4(string ipString)
        {
            if (String.IsNullOrWhiteSpace(ipString))
            {
                return false;
            }

            string[] splitValues = ipString.Split('.');
            if (splitValues.Length != 4)
            {
                return false;
            }

            return splitValues.All(r => byte.TryParse(r, out byte _));
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
