﻿using TeamDecided.RaftPrototype.Enums;

namespace TeamDecided.RaftPrototype.Models
{
    public class NodeObjectEventArgs
    {
        public int Index { get; set; }
        public ENodeField Field { get; set; }
        public string FieldValue { get; set; }
    }
}
