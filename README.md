## Prototype Demonstrator Project  
To demonstrate the various Use case requirements we have developed a prototype implmentation which can be downloaded [here](https://bitbucket.org/teamdecided/raftprototype/src/master/RaftPrototypeInstaller/Release/) 
  
This implementation includes the full list of Raft algorithm features described in the paper and the utilises our  
[Nuget package](https://www.nuget.org/packages/TeamDecided.RaftConsensus/) the easily distributable version of this [library](https://bitbucket.org/teamdecided/raftconsensuslibrary/src/master/)  
  
In our terminology, this prototype is a functional "UAS" (User Application Service). This prototype demonstrates the  
library's functionality by using our generic key value store interface IConsensus<TKey, TValue> with <string,string> as  
an example. It then maintains a distributed log accross multiple nodes following the Raft algorithm's protocol, including  
sustaining and recovering from minority node failure while keeping service active. This library uses our own UDP  
networking library, including an optional encryption layer.  
  
### Prototype Installation
1. Download Raft Prototype from [here](https://bitbucket.org/teamdecided/raftprototype/src/master/RaftPrototypeInstaller/Release/)
2. Run the installation msi, accept and warnings during installation process.
3. Installation is complete.

### Prototype Overview
After installation has complete you can run the RaftPrototype application. You will initially be presented with the  
RaftStarter window. This window allows for alternate flows through the cluster setup process.  
Create Cluster Config allows for easy customization of configuration details needed to create and run a cluster.  
![CreateClusterCong](https://cdn.discordapp.com/attachments/471607717056741387/488637374633345036/tempsnip.png)  

This interface allows you to quickly define the size and type of cluster to be setup.  
Key elements such as **use encryption** and **persistent storage** allow for alternate uses of the RaftConsensus library.  
The **Join Retry Attempts** allow us to increase the initial time for the cluster to form before a timeout occurs.  
Editable tabulated IP and port information allows for quick node network configuration. The default configuration assumes  
the nodes will all be run on the same computer for demonstrative purposes, meaning they'll all need their own ports.  
However, being IP based there is no reason why these nodes can't be run across the LAN, or even the Internet.  

![ClusterConfig](https://cdn.discordapp.com/attachments/471607717056741387/488639223294066701/unknown.png)  

To spawn multiple nodes, the program just needs to be opened multiple times; one for each node.  

### Use Case Demonstration With Prototype

#### Join Cluster

To implement the **Join cluster** use case the Prototype employs the Create Cluster Configuration workflow  
  
![ClusterConfig](https://cdn.discordapp.com/attachments/471607717056741387/488639223294066701/unknown.png)  
  
When cluster setup details are complete the build button initially prompts to save the configuration file for later use  
before proceeding to the StartNode window.  
  
![StartNode](https://cdn.discordapp.com/attachments/471607717056741387/488639023284224020/unknown.png)  
  
Clicking the Start button after selecting the approriate node from the drop down box will create the node.  
  
Alternately if you have an existing cluster you can load the configuration file directly with Start Existing Node.  
![StartExistingNode](https://cdn.discordapp.com/attachments/471607717056741387/488637610911072257/tempsnip1.png)  

Individual nodes are spawned by selection from the drop down box  
![StartNode](https://cdn.discordapp.com/attachments/471607717056741387/488639023284224020/unknown.png)  

This is an exmaple of the 3 nodes running, and having joined a into a cluster  
![Prototype Nodes](https://cdn.discordapp.com/attachments/471607717056741387/485423244749570051/nodes_sm.png)  
  
![RaftNode](https://cdn.discordapp.com/attachments/471607717056741387/488667471927312384/unknown.png)  
  
In the event that the node does not find the cluster in a timely manner an error will occur. The user is notified with the  
option to retry or cancel their attempt.  
![Retry](https://cdn.discordapp.com/attachments/471607717056741387/488680356497850378/tempsnip6.png)  
  
#### Append Entry
When the nodes instantiate an election takes place between members of the cluster, one of the nodes is elected to be UAS.  
Only the node that is the UAS  has ability to send **Append Entry** requests to the cluster.  
  
The User Application Sever status of the GUI is set to Acitve which alerts to the fact that this node is running the UAS  
and has the ability to send Append Entry requestsvia the input boxes which is no longer be greyed out allowing for string  
key value data to be entered. After data has been entered the **Append Entry** use case is completed by pressing the  
**Append** button.  
  
The **Append Entry** message is not commited to the log until consensus has been obtained from the cluster.

![Prototype Node1](https://cdn.discordapp.com/attachments/471607717056741387/485436013057933323/node_empty_sm.png)

#### Receive Commit Entries
The nodes display the updated log information after **Receive Commit Entries**

![Prototype Node1](https://cdn.discordapp.com/attachments/471607717056741387/488677655995088896/tempsnip2.png)  
  
#### Stop Node
Running nodes can be stop at anytime by pressing the **Stop Node** button  

![StopNode](https://cdn.discordapp.com/attachments/471607717056741387/488678876080504832/tempsnip4.png)  

#### Start Node
Stopped nodes can be started by pressing the **Start Node** button. The node will confirm the state of it's log and  
automatically append new entries.  
  
![StartNode](https://cdn.discordapp.com/attachments/471607717056741387/488678913359478794/tempsnip5.png)
  
#### Survive Node Failure
The below image shows a newly created cluster of three nodes. Node1 is the current UAS and each members log contains the  
message Hello/World.  
  
![CreatedCluster](https://cdn.discordapp.com/attachments/471607717056741387/488684239852929047/tempsnip8a.png)  
  
Node1 is taken down by pressing the Stop Node button, the image below shows that Node2 has won the elecetion is is now  
acting as the UAS.  
  
![NodeDown](https://cdn.discordapp.com/attachments/471607717056741387/488684241765531670/tempsnip8b.png)
  
Finally Node1 is restarted, the image below shows Node1 has it's **log rebuilt** and the cluster has successfully survived  
node failure.  
  
![Survive](https://cdn.discordapp.com/attachments/471607717056741387/488684246182133763/tempsnip8d.png)

#### Developer Read Log
The user is able to view the logs of the underlying consensus algorithm by selection the Log tab. Here they can see their  
consensus node state (leader/follower/candidate), see their debugging level, and see the auto scrolling log. The user is  
also able to change their debugging level as they'd like, if it's changed to Debug or Trace it's possible to see each  
heartbeat and message flow/processing.
  
![ReadLogs](https://cdn.discordapp.com/attachments/471607717056741387/489018332213674000/unknown.png)  
  
#### Node Start From Persistent Storage
The user is able to start the node loading log entries from persistent storage.
  
The image below shows Node1 is currently offline; the details displayed in its log window are a representation of the  
consensus log with each of these entries having been stored in the nodes persistent storage module.  
![PersistentStorageLoad](https://cdn.discordapp.com/attachments/471607717056741387/490334254232764416/tempsnip15.png)  
When the node is returned to online the consensus log is populated with commited information stored in the persistent  
storage database. This mechanic greatly reduces to overhead of collecting information from the cluster reducing the time  
to rebuild overall. The below image depicts the node1 rebuilt from persistent storage and then updated by the cluster.  
![PersistentStorageLoad](https://cdn.discordapp.com/attachments/471607717056741387/490334269336584212/tempsnip15b.png)  
***
